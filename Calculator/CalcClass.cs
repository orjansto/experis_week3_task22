﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyClasses
{
    public class CalcClass
    {
        public double Add(double a, double b)
        {
            double sum = a + b;
            if (double.IsInfinity(sum))
            {
                throw new ArgumentOutOfRangeException();
            }
            return (a + b);
        }
        public double Subtract(double a, double b)
        {
            return (a - b);
        }
        public double Multiply(double a, double b)
        {
            return (a * b);
        }
        public double Divide(double a, double b)
        {
            if (b == 0)
            {
                throw new ArgumentOutOfRangeException();
            }
            return (a / b);
        }
    }
}
