﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyClasses;
using Xunit;

namespace myNewTests
{
    public class myTests
    {
        [Fact]
        void add_testIfAddingRight()
        {
            //Arrange
            double a = 1;
            double b = 2;
            double expected = 3;
            double actual = 0;

            //Act
            CalcClass calc = new CalcClass();
            actual = calc.Add(a, b);

            //Assert
            Assert.Equal<double>(expected, actual);
        }
        [Fact]
        void add_testIfNumbersTooHigh()
        {
            //Arrange
            double a = double.MaxValue;
            double b = double.MaxValue;

            //Act
            CalcClass calc = new CalcClass();

            //Assert
            Assert.Throws<ArgumentOutOfRangeException>(() => calc.Add(a, b)); 
         }
        [Fact]
        void subtract_testIfSubtractingRight()
        {

            //Arrange
            double a = 4;
            double b = 2;
            double expected = 2;
            double actual = 0;

            //Act
            CalcClass calc = new CalcClass();
            actual = calc.Subtract(a, b);

            //Assert
            Assert.Equal<double>(expected, actual);

        }
        [Fact]
        void multiply_testIfMultyplyingRight()
        {
            //Arrange
            double a = 2;
            double b = 3;
            double expected = 6;
            double actual = 0;

            //Act
            CalcClass calc = new CalcClass();
            actual = calc.Multiply(a, b);

            //Assert
            Assert.Equal<double>(expected, actual);

        }
        [Fact]
        void divide_testIfDividingRight()
        {
            //Arrange
            double a = 6;
            double b = 2;
            double expected = 3;
            double actual = 0;

            //Act
            CalcClass calc = new CalcClass();
            actual = calc.Divide(a, b);

            //Assert
            Assert.Equal<double>(expected, actual);

        }
        [Fact]
        void divide_testIfDivideByZero()
        {
            //Arrange            
            double a = 6;
            double b = 0;
            
            //Act          
            CalcClass calc = new CalcClass();

            //Assert
            Assert.Throws<ArgumentOutOfRangeException>(() => calc.Divide(a, b));
        }
    }
}
