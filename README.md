﻿**Experis Academy, Norway**

**Authors:**
* **Ørjan Storås**

# Experis Week3 Task 22
## Task 22


- [x] Create a Winformsolution call MyTestingArea 
- [x] Add a project (Class Library) called myTests 
- [x] Write tests for the four methods that will be used in a basic calculator application (Add, Subtract, Multiply, Divide)
- [x] Then add another project (Class library). Write the four methods to satisfy the tests 
- [x] Think about situations where your methods will cause errors and write at least two tests for two possible failures (any of the methods). You can do one for one method and one for another metho