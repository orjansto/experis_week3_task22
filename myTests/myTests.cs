﻿using Calculator;
using System;
using System.Collections.Generic;
using System.Text;
using MyClasses;
using Xunit;

namespace myTests
{
    public class myTests
    {
        [Fact]
        void add_testIfAddingRight()
        {
            //Arrange
            //Set up test data
            double a = 1;
            double b = 2;
            double expected = 3;
            double actual = 0;

            //Act
            //Use the method in question

            //add a&b
            CalcClass calc = new CalcClass();
            actual = calc.Add(a, b);

            //Assert
            //Check that the results are correct
            Assert.Equal<double>(expected, actual);
        }
        [Fact]
        void subtract_testIfSubtractingRight()
        {
            
            //Arrange
            //Set up test data
            double a = 4;
            double b = 2;
            double expected = 2;
            double actual = 0;

            //Act
            //Use the method in question

            CalcClass calc = new CalcClass();
            actual = calc.Subtract(a, b);

            //Assert
            //Check that the results are correct
            Assert.Equal<double>(expected, actual);
            
        }
        [Fact]
        void multiply_testIfMultyplyingRight()
        {
            //Arrange
            //Set up test data
            double a = 2;
            double b = 3;
            double expected = 6;
            double actual = 0;

            //Act
            //Use the method in question
            CalcClass calc = new CalcClass();
            actual = calc.Multiply(a, b);

            //Assert
            //Check that the results are correct
            Assert.Equal<double>(expected, actual);
        
        }
        [Fact]
        void divide_testIfDividingRight()
        {
            //Arrange
            //Set up test data
            double a = 6;
            double b = 2;
            double expected = 3;
            double actual = 0;

            //Act
            //Use the method in question

            CalcClass calc = new CalcClass();
            actual = calc.Divide(a, b);

            //Assert
            //Check that the results are correct
            Assert.Equal<double>(expected, actual);
            
        }
    }
}
